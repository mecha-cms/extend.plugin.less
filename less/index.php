<?php namespace _\art;

if (\extension('less') === null) {
    \Guard::abort('Missing <code>less</code> extension.');
}

function less($content, array $lot = []) {
    if (!$path = $this->path) {
        return $content;
    }
    $css = \Path::F($path) . DS . 'css.data';
    if (!$content || !\is_file($css)) {
        return $content;
    }
    $less = new \lessc;
    $less->importDisabled = true; // Disable `@import` rule
    $less->setFormatter('compressed');
    if ($function = \extension('less:function')) {
        foreach ((array) $function as $k => $v) {
            $less->registerFunction($k, $v);
        }
    }
    if ($variable = \extension('less:variable')) {
        $less->setVariables((array) $variable);
    }
    $cache = \str_replace(ROOT, CACHE, $css);
    $convert = function(string $content) use(&$less): string {
        if (\strpos($content, '</style>') !== false) {
            return \preg_replace_callback('#<style(\s[^>]*)?>\s*([\s\S]*?)\s*</style>#', function($m) use($less) {
                $content = \trim($less->compile($m[2]));
                if (\extension('minify') !== null) {
                    $content = \Minify::CSS($content); // Optimize where possible
                } else {
                    $content = N . $content . N;
                }
                return '<style' . $m[1] . '>' . $content . '</style>';
            }, $content);
        }
        $content = \trim($less->compile($content));
        if (\extension('minify') !== null) {
            $content = \Minify::CSS($content); // Optimize where possible
        }
        return $content;
    };
    if (\plugin('less')['cache']) {
        if (!\is_file($cache) || \filemtime($css) > \filemtime($cache)) {
            \File::set($content = $convert($content))->saveTo($cache, 0600);
            return $content;
        }
        return \file_get_contents($cache);
    }
    return $convert($content);
}

\Hook::set('*.css', __NAMESPACE__ . "\\less", 2);